# gosh

a very basic skeleton for a shell written in golang.

## features

* pwd - print current working dir
* ls - list files
* cd - change directory (does nothing)
* exit - leave the shell

## missing

* support for arguments to commands 
* more commands

## source

* https://medium.com/@BugDiver/lets-write-a-shell-from-scratch-using-golang-part-i-e528ff6ee6f8

consider re-writing, as outlined here:
* https://simjue.pages.dev/post/2018/07-01-go-unix-shell/
